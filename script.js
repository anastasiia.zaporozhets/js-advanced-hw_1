"use strict"

class Employee {
    constructor(name, age,salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get name (){
        return this._name;
    }
    set name (newName){
        this._name = newName;
    }

    get age(){
        return this._age;
    }
    set age (newAge){
        this._age = newAge;
    }

    get salary (){
        return this._salary ;
    }

    set salary (newSalary){
        this._salary = newSalary;
    }
}


class Programmer extends Employee {
    constructor(name, age,salary,lang) {
        super(name, age,salary);
        this.lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }

    get salary() {
        return this._salary * 3;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

let programmer = new Programmer("Anna", 25, 10000, ["Java", "Python"]);
console.log(programmer);

let programmer2 = new Programmer("Vika", 35, 40000, ["Java Script"])
console.log(programmer2);

let programmer3 = new Programmer("Nick", 30, 17000, ["Python","Java Script","Java"])
console.log(programmer3);



